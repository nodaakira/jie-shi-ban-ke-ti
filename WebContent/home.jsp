<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
<link rel="stylesheet" href="./style.css">

<script type="text/javascript">
	function disp() {
		//
		if (window.confirm('投稿を削除しますか？')) {
			return true;
		} else {
			window.alert('キャンセルされました');
			return false;
		}
	}
	function disp2() {
		//
		if (window.confirm('コメントを削除しますか？')) {
			return true;
		} else {
			window.alert('キャンセルされました');
			return false;
		}
	}
</script>
</head>
<body>
	<div class="center">

	<p class="right">
		<c:if test="${ loginUser.position_id == 1 }">
		<a  href="manage" style="text-align: right ;">ユーザー管理</a>
		</c:if>   　　　
		<a  href="logout" style="text-align: right ;">ログアウト</a>
	</p>

	<div class="main-contents">
		<div class="title"><h3>ホーム画面</h3></div>
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">


				<c:set var="data" value="現在${loginUser.name}でログインしています" />
				<c:out value="${data}" />

			</c:if>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">

					<c:forEach items="${errorMessages}" var="messages">
						<li><c:out value="${messages}" />
					</c:forEach>

			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<a href="post">新規投稿</a>

		<form action="./" method="get">
			<input type="text" placeholder=カテゴリ検索 name="category" value="${category}"> <input
				type="date" name="day1" value="${day1}">～ <input type="date"
				name="day2" value="${day2}"> <input type="submit"
				name="test" value="検索">

		</form>
		<form action="./" method="post">
		<input type="submit"
				name="test" value="検索リセット">
				</form>

		<div class="posts">
			<c:forEach items="${posts}" var="post">
				<div class="box_sample01">



						<div class="subject">
							<c:out value="[${post.subject}]" />
						</div>
						<div class="text">
							<pre><c:out value="${post.text}" /></pre>
						</div>
						<div class="category">
							<c:out value="[${post.category}]" />
						</div>
						<div class="created_date">
							<fmt:formatDate value="${post.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

					<div class="name">
						<c:out value="${post.name}" />
						さんの投稿
					</div>

					<form action="delete_post" method="post">
						<input type="hidden" name="delete_id" value="${post.id}">
						<c:if test="${post.user_id == loginUser.id}">
						<p>
							<button type="submit" value="投稿削除" onClick="return disp();">投稿削除</button>
						</p>
						</c:if>
					</form>

						<c:forEach items="${comments}" var="comment">

							<c:if test="${comment.post_id == post.id}">
								<div class="box_sample03">
									<pre><c:out value="${comment.text}" /></pre>

								<div class="created_date">
									<fmt:formatDate value="${comment.created_date}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
								<div class="name">
									<c:out value="${comment.name}" />
									さんのコメント
								</div>
								<form action="delete_comment" method="post">
									<input type="hidden" name="delete_commentId"
										value="${comment.id}">
										<c:if test="${comment.user_id == loginUser.id}">
									<p>
										<button type="submit" value="コメント削除" onClick="return disp2();">コメント削除</button>
									</p>
									</c:if>
								</form>
								</div>
							</c:if>

						</c:forEach>

					<div class="form-area">

						<form action="comment" method="post">
							<input type="hidden" name="post_id" value="${post.id}">
							<textarea name="text" cols="50" rows="6" class="tweet-box"></textarea>
							<br /> <input type="submit" value="コメント">（500文字まで）
						</form>

					</div>
				</div>
			</c:forEach>
		</div>
		<div class="copylight">Copyright(c)Akira Noda</div>
	</div>
	</div>
</body>
</html>
