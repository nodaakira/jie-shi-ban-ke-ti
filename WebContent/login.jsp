<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="./style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
<link rel="stylesheet" href="./style.css">
</head>
<body>
<div class="center">
	<div class="main-contents">
		<div class="title"><h3>社内掲示板</h3></div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">

					<c:forEach items="${errorMessages}" var="messages">
						<li><c:out value="${messages}" />
					</c:forEach>

			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="login" method="post">
			<br /> <label for="login_id">ログインＩＤ</label>
			<input name="login_id" value="${login_id}"
				id="login_id" /> <br /> <label for="password">パスワード</label> <input
				name="password" type="password" id="password" /> <br /> <input
				type="submit" value="ログイン" /> <br /> <input type="hidden"
				name="is_deleted" value=0>
		</form>
		<div class="copylight">Copyright(c)Akira Noda</div>
	</div>
	</div>
</body>
</html>
