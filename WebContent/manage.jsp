<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="./style.css">

<title>ユーザー管理</title>

<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
	function disp() {
		//
		if (window.confirm('アカウントを停止しますか？')) {
			return true;
		} else {
			window.alert('キャンセルされました');
			return false;
		}
	}
	function disp2() {
		//
		if (window.confirm('アカウントを復活させますか？')) {
			return true;
		} else {
			window.alert('キャンセルされました');
			return false;
		}
	}
</script>

</head>
<body>
<div class="center">

	<p class="right">
	<a href="signup">ユーザー登録</a>　　　
	<a href="./">戻る</a>
	</p>

	<div class="title"><h3>ユーザー管理画面</h3></div>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="users">

		<table border="1">

			<tr>
				<th>[ユーザーＩＤ]</th>
				<th>[名称]</th>
				<th>[支店]</th>
				<th>[役職]</th>
				<th>[アカウント]</th>
				<th></th>

			</tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td>
						<div class="id">
							<c:out value="${user.id}" />
						</div>
					</td>
					<td>
						<div class="name">
							<c:out value="${user.name}" />
						</div>
					</td>
					<td>
						<div class="branch_id">
							<c:out value="${user.branch}" />
						</div>
					</td>
					<td>
						<div class="position_id">
							<c:out value="${user.position}" />
						</div>
					</td>
					<td>
						<form action="manage" method="post">
							<input type="hidden" name="ban_userId" value="${user.id}">
							<input type="hidden" name="is_deleted" value="${user.is_deleted}">
							<c:if test="${user.is_deleted == 0 }">
								<c:if test="${user.id != loginUser.id}">
									<p>
										<button type="submit" value="停止" id="ban" onClick="return disp();">停止</button>
									</p>
								</c:if>
								<c:if test="${user.id == loginUser.id}">
									<p>
										ログイン中です
									</p>
								</c:if>
							</c:if>
						</form>
						<form action="return" method="post">
							<input type="hidden" name="ban_userId" value="${user.id}">
							<input type="hidden" name="is_deleted" value="${user.is_deleted}">
							<c:if test="${user.is_deleted == 1 }">
								<c:if test="${user.id != loginUser.id}">
									<p>
										<button type="submit" value="復活" id="return" onClick="return disp2();">復活</button>
									</p>
								</c:if>
							</c:if>
						</form>
					</td>
					<td>
						<form action="edit" method="get">
							<input type="hidden" name="edit_userId" value="${user.id}">
							<input type="submit" value="編集">
						</form>
					</td>
				</tr>
			</c:forEach>

		</table>

	</div>
	<div class="copyright">Copyright(c)Akira Noda</div>
	</div>
</body>
</html>