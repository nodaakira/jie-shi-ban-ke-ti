<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<link rel="stylesheet" href="./style.css">
</head>
<body>
	<div class="main-contents">
		<div class="title"><h3>ユーザー登録画面</h3></div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /> <label for="login_id">ログインＩＤ</label> <input name="login_id"
				value="${profUser.login_id}" id="login_id" />(6文字以上20文字以下の半角英数字) <br />

			<label for="password">パスワード</label> <input name="password"
				type="password" id="password" />(6文字以上20文字以下の記号を含む全ての半角文字) <br />

			<label for="checkpassword">確認用パスワード</label> <input
				name="checkpassword" type="password" id="checkpassword" /> <br />

			<label for="name">名称</label> <input name="name"
				value="${profUser.name}" id="name" />(10文字以下) <br /> <label
				for="branch_id">支店</label>
			<!--  <input name="branch_id" value="${profUser.branch_id}" id="branch_id" /> <br /> -->
			<select name="branch_id">
				<c:forEach items="${branch}" var="branches">

					<c:if test="${profUser.branch_id == branches.id }">
						<option value="${branches.id}" selected>${branches.branch}</option>
					</c:if>

					<c:if test="${profUser.branch_id != branches.id }">
						<option value="${branches.id}">${branches.branch}</option>
					</c:if>

				</c:forEach>
			</select> <br /> <label for="position_id">部署・役職</label>
			<!--  <input name="position_id" value="${profUser.position_id}" id="position_id"> <br /> -->
			<select name="position_id">
				<c:forEach items="${position}" var="positions">

					<c:if test="${profUser.position_id == positions.id }">
						<option value="${positions.id}" selected>${positions.position}</option>
					</c:if>

					<c:if test="${profUser.position_id != positions.id }">
						<option value="${positions.id}">${positions.position}</option>
					</c:if>

				</c:forEach>
			</select> <br /> <input type="submit" value="登録" /> <br /> <a href="manage">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Akira Noda</div>
	</div>
</body>
</html>