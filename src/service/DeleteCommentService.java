package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Post;
import dao.DeleteCommentDao;

public class DeleteCommentService {

	public Post delete_commentId(String delete_commentId) {

		Connection connection = null;
		try {
			connection = getConnection();

			DeleteCommentDao DeleteCommentDao = new DeleteCommentDao();

			Post post = DeleteCommentDao.delete(connection, delete_commentId );

			commit(connection);

			return post;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
