package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Position;
import dao.ManageDao;

public class ManageService {

    public Position getPosition(String userPosition) {

		Connection connection = null;
		try {
			connection = getConnection();

			ManageDao ManageDao = new ManageDao();
			Position position = ManageDao.select(connection, userPosition);

			commit(connection);

			return position;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}



