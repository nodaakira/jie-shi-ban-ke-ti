package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Post;
import dao.DeletePostDao;

public class DeletePostService {

	public Post delete_id(String delete_id) {

		Connection connection = null;
		try {
			connection = getConnection();

			DeletePostDao DeletePostDao = new DeletePostDao();

			Post post = DeletePostDao.delete(connection, delete_id );

			commit(connection);

			return post;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
