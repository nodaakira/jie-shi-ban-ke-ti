package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.UserPost;
import dao.PostDao;
import dao.UserPostDao;

public class PostService {

	public void register(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao PostDao = new PostDao();
			PostDao.insert(connection, post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserPost> getPost(String day1, String day2, String category) {

		Connection connection = null;
		try {
			connection = getConnection();

			String day1time;
			String day2time;

			if(StringUtils.isEmpty(day1) == true ){

				day1time = ( " 2018/01/01 00:00:00");

			}else{

				String time1 = ( " 00:00:00" );
				day1time = day1 + time1;


			}
			if(StringUtils.isEmpty(day2) == true){

		        Date date = new Date();
		        day2time = new SimpleDateFormat(" yyyy/MM/dd HH:mm:ss").format(date);

			}else{


				String time2 = ( " 23:59:59" );

				day2time = day2 + time2;

			}

			UserPostDao PostDao = new UserPostDao();
			List<UserPost> ret = PostDao.getPostDay(connection, day1time, day2time, category, LIMIT_NUM);
			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	public List<UserPost> search(String day1, String day2, String category) {
//
//
//		Connection connection = null;
//		try {
//			connection = getConnection();
//
//			String day1time;
//			String day2time;
//
//			if(StringUtils.isEmpty(day1) == true && StringUtils.isEmpty(day2) == true){
//
//				day1time = ( " 2018/01/01 00:00:00");
//		        Date date = new Date();
//		        day2time = new SimpleDateFormat(" yyyy/MM/dd HH:mm:ss").format(date);
//
//			}else{
//
//				String time1 = ( " 00:00:00" );
//				String time2 = ( " 23:59:59" );
//				day1time = day1 + time1;
//				day2time = day2 + time2;
//
//			}
//			UserPostDao PostDao = new UserPostDao();
//			List<UserPost> ret = PostDao.getPostDay(connection, day1time, day2time, category, LIMIT_NUM);
//			commit(connection);
//			return ret;
//
//		} catch (RuntimeException e) {
//			rollback(connection);
//			throw e;
//		} catch (Error e) {
//			rollback(connection);
//			throw e;
//		} finally {
//			close(connection);
//		}
//	}
}
