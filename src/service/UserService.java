package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;


public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<User> getUser() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao UserDao = new UserDao();
			List<User> ret = UserDao.getUserInfo(connection);

			commit(connection);

			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    public User getEditUser(String edit_userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao UserDao = new UserDao();
			User user = UserDao.getUser(connection, edit_userId);
			commit(connection);

			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(!user.getPassword().isEmpty()){
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);
            }
            //System.out.println("aaaa");

            UserDao UserDao = new UserDao();
            UserDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void userBan(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao UserDao = new UserDao();
            UserDao.userBan(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void userReturn(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao UserDao = new UserDao();
            UserDao.userReturn(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User getUserId(String login_id) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao UserDao = new UserDao();
			User user = UserDao.getUserId(connection, login_id);
			commit(connection);

			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}






