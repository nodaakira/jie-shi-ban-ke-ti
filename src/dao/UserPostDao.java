package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserPost;
import exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPosts(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("Posts.id as id, ");
			sql.append("Posts.user_id as user_id, ");
			sql.append("Posts.subject as subject, ");
			sql.append("Posts.text as text, ");
			sql.append("Posts.category as category, ");
			sql.append("Posts.created_date as created_date ");
			sql.append("users.name as name, ");
			sql.append("FROM Posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON Posts.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());
			System.out.println(ps);

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserPost> toUserPostList(ResultSet rs) throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp created_date = rs.getTimestamp("created_date");

				UserPost Post = new UserPost();
				Post.setId(id);
				Post.setUser_id(user_id);
				Post.setSubject(subject);
				Post.setText(text);
				Post.setCategory(category);
				Post.setCreated_date(created_date);
				ret.add(Post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<UserPost> getPostDay(Connection connection, String day1time, String day2time, String category,
			int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("Posts.id as id, ");
			sql.append("Posts.user_id as user_id, ");
			sql.append("Posts.subject as subject, ");
			sql.append("Posts.text as text, ");
			sql.append("Posts.category as category, ");
			sql.append("Posts.created_date as created_date, ");
			sql.append("users.name as name ");
			sql.append("FROM Posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON Posts.user_id = users.id ");
			sql.append("WHERE  ( Posts.created_date >= ? AND Posts.created_date <= ? )");
			if (category != null) {
				sql.append("AND category LIKE ? ");
			}
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, day1time);
			ps.setString(2, day2time);
			if (category != null) {
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toPostDayList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserPost> toPostDayList(ResultSet rs) throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				String name = rs.getString("name");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp created_date = rs.getTimestamp("created_date");

				UserPost Post = new UserPost();
				Post.setId(id);
				Post.setUser_id(user_id);
				Post.setName(name);
				Post.setSubject(subject);
				Post.setText(text);
				Post.setCategory(category);
				Post.setCreated_date(created_date);
				ret.add(Post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}