package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {

	public List<Position> getPosition(Connection connection) {

		PreparedStatement ps = null;
		try {

			String sql = "SELECT * FROM positions ";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Position> ret = toPositionList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Position> toPositionList(ResultSet rs) throws SQLException {

		List<Position> ret = new ArrayList<Position>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String positions = rs.getString("position");
				Timestamp created_date = rs.getTimestamp("created_date");
				Timestamp updated_date = rs.getTimestamp("updated_date");
				Position position = new Position();
				position.setId(id);
				position.setPosition(positions);
				position.setCreated_date(created_date);
				position.setUpdated_date(updated_date);
				ret.add(position);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
