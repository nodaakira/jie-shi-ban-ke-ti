package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Position;
import exception.SQLRuntimeException;

public class ManageDao {

	public Position select(Connection connection, String userPosition) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("positions.id as id, ");
			sql.append("positions.name as name ");
			sql.append("FROM positions ");

			ps = connection.prepareStatement(sql.toString());

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
		return null;
	}

}