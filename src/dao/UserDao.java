package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	public User getUser(Connection connection, String login_id, String password, String is_deleted) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ?  AND password = ? AND is_deleted = 0 ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (10 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branch_id = rs.getString("branch_id");
				String position_id = rs.getString("position_id");
				String is_deleted = rs.getString("is_deleted");
				Timestamp created_date = rs.getTimestamp("created_date");
				Timestamp updated_date = rs.getTimestamp("updated_date");
				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_id(branch_id);
				// user.setBranch(branch);
				user.setPosition_id(position_id);
				// user.setPosition(position);
				user.setIs_deleted(is_deleted);
				user.setCreated_date(created_date);
				user.setUpdated_date(updated_date);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<User> toGetList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branch_id = rs.getString("branch_id");
				String branch = rs.getString("branch");
				String position_id = rs.getString("position_id");
				String position = rs.getString("position");
				String is_deleted = rs.getString("is_deleted");
				Timestamp created_date = rs.getTimestamp("created_date");
				Timestamp updated_date = rs.getTimestamp("updated_date");
				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_id(branch_id);
				user.setBranch(branch);
				user.setPosition_id(position_id);
				user.setPosition(position);
				user.setIs_deleted(is_deleted);
				user.setCreated_date(created_date);
				user.setUpdated_date(updated_date);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			// sql.append("id");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			// //sql.append("NEXT VALUE FOR my_seq "); // id
			sql.append("?"); // login_id
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // position_id
			sql.append(", CURRENT_TIMESTAMP"); // create_date
			sql.append(", CURRENT_TIMESTAMP"); // update_date
			// sql.append("WHERE login_id != ?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setString(4, user.getBranch_id());
			ps.setString(5, user.getPosition_id());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> getUserInfo(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM ( users INNER JOIN branches ON users.branch_id = branches.id ) ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("ORDER BY users.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<User> ret = toGetList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String edit_userId) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setObject(1, edit_userId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE  users SET  ");
			// sql.append("id");
			sql.append("login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			if (!user.getPassword().isEmpty()) {
				sql.append(", password = ?");
			}
			sql.append("WHERE id = ?");
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setString(3, user.getBranch_id());
			ps.setString(4, user.getPosition_id());
			if (!user.getPassword().isEmpty()) {
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			} else {
				ps.setInt(5, user.getId());
			}
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void userBan(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE  users SET  ");
			// sql.append("id");
			sql.append("is_deleted = 1 ");
			sql.append("WHERE id = ?");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void userReturn(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE  users SET  ");
			// sql.append("id");
			sql.append("is_deleted = 0 ");
			sql.append("WHERE id = ?");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUserId(Connection connection, String login_id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? ";

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, login_id);

			ResultSet rs = ps.executeQuery();

			List<User> ret = toUserList(rs);

			if (ret.isEmpty() == true) {
				return null;
			} else if (2 <= ret.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return ret.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
}
