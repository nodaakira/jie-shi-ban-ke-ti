package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.post_id as post_id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.created_date as created_date, ");
			sql.append("users.name as name ");
			sql.append("FROM ( comments INNER JOIN users ON comments.user_id = users.id ) ");
			sql.append("INNER JOIN posts ");
			sql.append("ON comments.post_id = posts.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs) throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int post_id = rs.getInt("post_id");
				int user_id = rs.getInt("user_id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Timestamp created_date = rs.getTimestamp("created_date");

				UserComment Post = new UserComment();
				Post.setId(id);
				Post.setUser_id(user_id);
				Post.setPost_id(post_id);
				Post.setName(name);
				Post.setText(text);
				Post.setCreated_date(created_date);
				ret.add(Post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}