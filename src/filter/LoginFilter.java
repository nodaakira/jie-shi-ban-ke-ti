package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns = { "/*" })
public class LoginFilter implements Filter {

	// public static String loginUser = "loginUser";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
//		try {

			Object session1 = ((HttpServletRequest) request).getSession().getAttribute("loginUser");
			String path = ((HttpServletRequest) request).getServletPath();
			HttpSession session = ((HttpServletRequest)request).getSession();

			// User user = (User) ((HttpServletRequest)request).getSession();

			if (session1 == null && !(path.equals("/login")) && !(path.equals("./style.css")) ) {
//				HttpSession session1 = ((HttpServletRequest) request).getSession();
				// session1 = ((HttpServletRequest)request).getSession();
				// session1.setAttribute("loginUser", user);
				List<String> messages = new ArrayList<String>();
				System.out.println(path);
				messages.add("ログインしてください");
				session.setAttribute("errorMessages", messages);

				((HttpServletResponse) response).sendRedirect("login");

				return;
			}
			chain.doFilter(request, response);
//		} catch (ServletException se) {
//		}
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}
