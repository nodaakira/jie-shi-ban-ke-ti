package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = { "/manage", "/edit", "/signup" })
public class manageFilter implements Filter {

	// public static String loginUser = "loginUser";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

			 User session1 = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");
			 HttpSession session = ((HttpServletRequest)request).getSession();

			if (!session1.getPosition_id().equals("1") ) {
				((HttpServletResponse) response).sendRedirect("./");

				List<String> messages = new ArrayList<String>();
				messages.add("権限がありません");
				session.setAttribute("errorMessages", messages);

				return;
			}
			chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}
