package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserComment;
import beans.UserPost;
import service.CommentService;
import service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");

		String day1 = request.getParameter("day1");
		String day2 = request.getParameter("day2");
		String category = request.getParameter("category");

		List<UserPost> posts = new PostService().getPost(day1, day2, category);
		List<UserComment> comments = new CommentService().getComment();

		request.setAttribute("comments", comments);
		request.setAttribute("posts", posts);
		request.setAttribute("day1", day1);
		request.setAttribute("day2", day2);
		request.setAttribute("category", category);
		request.setAttribute("userId", user.getId());

		System.out.println();

		System.out.println(user.getId());

		ServletContext context = request.getServletContext();
		RequestDispatcher rd = context.getRequestDispatcher("/home.jsp");
		rd.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.sendRedirect("./");

	}
}