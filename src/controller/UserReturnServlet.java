package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/return" })
public class UserReturnServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User user = new User();
		int id = Integer.parseInt(request.getParameter("ban_userId"));
		user.setId(id);
		user.setIs_deleted(request.getParameter("is_deleted"));
		System.out.println(id);
		System.out.println(request.getParameter("is_deleted"));
		new UserService().userReturn(user);
		response.sendRedirect("manage");

	}
}