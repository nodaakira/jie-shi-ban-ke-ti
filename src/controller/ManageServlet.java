package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/manage" })
public class ManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<User> users = new UserService().getUser();

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowPostForm;
		if (user != null) {
			isShowPostForm = true;
		} else {
			isShowPostForm = false;
		}

		request.setAttribute("users", users);
		request.setAttribute("isShowPostForm", isShowPostForm);

		request.getRequestDispatcher("manage.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User user = new User();
		int id = Integer.parseInt(request.getParameter("ban_userId"));
		user.setId(id);
		user.setIs_deleted(request.getParameter("is_deleted"));

		new UserService().userBan(user);
		response.sendRedirect("manage");

	}
}