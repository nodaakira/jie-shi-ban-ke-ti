package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();

		request.setAttribute("branch", branch);
		request.setAttribute("position", position);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User user = new User();
		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();

		user.setLogin_id(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch_id(request.getParameter("branch_id"));
		user.setPosition_id(request.getParameter("position_id"));

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			new UserService().register(user);

			response.sendRedirect("manage");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("profUser", user);
			request.setAttribute("branch", branch);
			request.setAttribute("position", position);

			ServletContext context = request.getServletContext();
			RequestDispatcher rd = context.getRequestDispatcher("/signup.jsp");
			rd.forward(request, response);

		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String login_id = request.getParameter("login_id");

		UserService UserService = new UserService();

		User user = UserService.getUserId(login_id);

		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String checkpassword = request.getParameter("checkpassword");
		String branch = request.getParameter("branch_id");
		String position = request.getParameter("position_id");
		System.out.println(branch);

		if (user != null) {
			messages.add("ログインＩＤが重複しています");
		}
		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインＩＤを入力してください");
		}
		if (!login_id.matches("^\\w{6,20}$") && !login_id.equals("")) {
			messages.add("ログインＩＤは6文字以上20文字以下の半角英数字で入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if (!password.matches("^[a-zA-Z0-9 -/:-@\\[-`{-~]{6,20}+$") && !password.equals("")) {
			messages.add("パスワードは6文字以上20文字以下の記号を含む全ての半角文字で入力してください");
		}
		if (!checkpassword.equals(password)) {
			messages.add("パスワードが一致しません");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名称を入力してください");
		}
		if (!name.matches("^.{1,10}$") && !name.equals("")) {
			messages.add("名前は10文字以内で入力してください");
		}
		if (branch == null) {
			messages.add("支店を選択してください");
		}
		if (StringUtils.isEmpty(position) == true) {
			messages.add("役職を選択してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
